# This file was created by bin/epadmin
# You can regenerate this file by doing ./bin/epadmin config_core template
$c->{host} = 'eprints.deb';
$c->{port} = 80;
$c->{aliases} = [];
$c->{securehost} = '';
$c->{secureport} = 443;
$c->{http_root} = undef;
